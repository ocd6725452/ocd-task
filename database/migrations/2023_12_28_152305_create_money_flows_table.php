<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('money_flows', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->unsignedBigInteger('money_flowable_id');
            $table->string('money_flowable_type');
            $table->enum('direction', ['income', 'outcome']);
            $table->float('price');
            $table->float('discount_ammount');
            $table->float('total_price');
            $table->float('paid_price');
            $table->boolean('is_repeatative');
            $table->boolean('is_repeatated');
            $table->integer('repeatative_day');
            $table->boolean('is_permanent');
            $table->date('valid_from');
            $table->date('valid_to');
            $table->date('date');
            $table->date('due_date');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('money_flows');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('branch_money_flow', function (Blueprint $table) {
            $table->id();
            $table->enum('direction', ['income', 'outcome']);
            $table->foreignId('branch_id')->constrained()->onDelete('cascade');
            $table->foreignId('money_flow_id')->constrained()->onDelete('cascade');
            $table->float('price', 10, 2);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('branch_money_flow');
    }
};

<?php

namespace App\Models;

use App\Models\MoneyFlow;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Wallet extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name', 'description'];

    public function moneyFlows() {
        return $this->belongsToMany(MoneyFlow::class)
        ->withPivot('description', 'direction', 'price', 'date')
        ->withTimestamps();
    }
}

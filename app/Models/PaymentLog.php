<?php

namespace App\Models;

use App\Models\MoneyFlow;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PaymentLog extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'direction',
        'money_flow_id',
        'price',
        'date'
    ];

    public function moneyFlow() {
        return $this->belongsTo(MoneyFlow::class);
    }
}

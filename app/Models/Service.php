<?php

namespace App\Models;

use App\Models\Branch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Service extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name', 'description', 'branch_id', 'is_monthly', 'month_from', 'month_to', 'start_from', 'finished_at'];

    public function branch() {
        return $this->belongsTo(Branch::class);
    }

    public function moneyFlows() {
        return $this->morphMany(MoneyFlow::class, 'money_flowable');
    }
}

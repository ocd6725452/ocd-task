<?php

namespace App\Models;

use App\Models\Wallet;
use App\Models\PaymentLog;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MoneyFlow extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'money_flowable_id',
        'money_flowable_type',
        'direction',
        'price',
        'discount_ammount',
        'total_price',
        'paid_price',
        'is_repeatative',
        'is_repeatated',
        'repeatative_day',
        'is_permanent',
        'valid_from',
        'valid_to',
        'date',
        'due_date'
    ];

    public function moneyFlowable() {
        return $this->morphTo();
    }

    public function paymentLogs() {
        return $this->hasMany(PaymentLog::class);
    }

    public function wallets() {
        return $this->belongsToMany(Wallet::class)
        ->withPivot('description', 'direction', 'price', 'date')
        ->withTimestamps();
    }

    public function branches() {
        return $this->belongsToMany(Branch::class)
        ->withPivot('direction', 'price')
        ->withTimestamps();
    }
}

<?php

namespace App\Models;

use App\Models\Service;
use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Branch extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name', 'description'];

    public function services() {
        return $this->hasMany(Service::class);
    }

    public function employees() {
        return $this->hasMany(Employee::class);
    }

    public function moneyFlows() {
        return $this->belongsToMany(MoneyFlow::class)
        ->withPivot('direction', 'price')
        ->withTimestamps();
    }
}

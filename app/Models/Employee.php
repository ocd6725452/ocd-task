<?php

namespace App\Models;

use App\Models\Branch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name', 'description', 'branch_id', 'is_monsthly', 'salary_day', 'start_from', 'finished_at'];

    public function branch() {
        return $this->belongsTo(Branch::class);
    }

    public function moneyFlows() {
        return $this->morphMany(MoneyFlow::class, 'money_flowable');
    }
}
